# FROM alpine:latest
# We will come back to alpine once they fix the unability to run sanitizers
# The issue is: the runtime 
FROM ubuntu:latest
COPY to-docker-image/* .

# The `ln` part is not needed on Alpine linux
RUN apt-get update && apt-get -y install clang-11 git binutils make clang-tidy-11 python nasm && \
    ln -s /bin/clang-11 /bin/clang && \
    ln -s /bin/clang-tidy-11 /bin/clang-tidy
# RUN apk update && apk upgrade && \
#   apk add bash clang-11 git binutils clang-extra-tools musl-dev make

RUN chmod 700 /*.sh

